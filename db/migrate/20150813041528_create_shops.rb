class CreateShops < ActiveRecord::Migration
  def change
    create_table :shops do |t|
      t.belongs_to :owner, index: true
      t.string :title
      t.string :subdomain
      t.integer :selected_design_id, default: 1
      t.boolean :is_offline, default: false
      t.text :offline_message

      t.timestamps null: false
    end
  end
end
