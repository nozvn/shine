class CreateVariants < ActiveRecord::Migration
  def change
    create_table :variants do |t|
      t.belongs_to :product, index: true, foreign_key: true
      t.integer :price
      t.boolean :is_master, default: false
      t.string :option1_value
      t.string :option2_value
      t.string :option3_value

      t.timestamps null: false
    end
  end
end
