class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.string :slug
      t.string :state
      t.string :option1
      t.string :option2
      t.string :option3
      t.string :description
      t.text :body
      t.datetime :deleted_at

      t.timestamps null: false
    end
  end
end
