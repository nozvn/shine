class CreateDesignSettings < ActiveRecord::Migration
  def change
    create_table :design_settings do |t|
      t.belongs_to :design, index: true, foreign_key: true
      t.string :name
      t.string :value

      t.timestamps null: false
    end
  end
end
