class AddTrackInventoryToVariants < ActiveRecord::Migration
  def change
    add_column :variants, :track_inventory, :boolean, default: false
    add_column :variants, :allow_backorder, :boolean, default: false
    add_column :variants, :in_stock, :integer
    add_column :variants, :alert_inventory_threshold, :integer, default: 0
  end
end
