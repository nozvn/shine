class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :number
      t.string :email
      t.string :ip_address
      t.belongs_to :billing_address, index: true
      t.belongs_to :shipping_address, index: true
      t.datetime :completed_at

      t.timestamps null: false
    end
  end
end
