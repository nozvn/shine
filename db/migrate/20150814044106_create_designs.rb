class CreateDesigns < ActiveRecord::Migration
  def change
    create_table :designs do |t|
      t.belongs_to :shop, index: true, foreign_key: true
      t.string :name
      t.string :slug
      t.text :description
      t.string :author
      t.string :email
      t.string :website
      t.string :version

      t.timestamps null: false
    end
  end
end
