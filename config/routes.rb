require 'constraints/subdomain/shop'
require 'constraints/subdomain/checkout'

Rails.application.routes.draw do
  devise_for :users
  use_doorkeeper

  constraints(Constraints::Subdomain::Shop) do
    scope 'backend', module: :shop_backend, as: :backend do
      resources :products
      resources :categories
      resources :pages
      resources :orders

      get '/settings' => "settings#index", as: :settings

      root to: "base#index"
    end

    namespace :api, defaults: {format: 'json'} do
      namespace :v1 do

        resources :categories do
          collection do
            post 'reorder'
          end
        end

        resources :orders
        resources :products
      end
    end

    scope module: :shop_frontend do
      get  '/'                        => "home#index",      as: 'home'

      get  '/categories/:id'          => "categories#show",  as: 'category'

      get  '/products'                => "products#index",   as: 'products'
      get  '/products/:id'            => "products#show",    as: 'product'

      get  '/cart'                    => "cart#show",        as: 'cart'
      post '/cart'                    => "cart#update"
      get  '/cart/empty'              => "cart#empty"

      post '/add_item'                => "cart#add_item"
      get  '/remove_item/:variant_id' => "cart#remove_item"

      constraints format: :txt do
        get  '/robots'  => "robots#index"
      end

      constraints format: :xml do
        get '/sitemap'            => "sitemaps#index"
        get '/sitemap_products'   => "sitemaps#products"
        get '/sitemap_categories' => "sitemaps#categories"
      end

      get  '/:id'                     => "pages#show",       as: 'page'
    end
  end

  scope module: :shop_frontend do
    get '/data/:shop_id/designs/:design_id/assets/:name.:format',
      to: 'assets#show', name: /.+/
  end

  constraints(Constraints::Subdomain::Checkout) do
    scope module: :shop_frontend do
      get  '/:shop_id/:cart_id' => "checkout#index", as: 'checkout'
      post '/:shop_id/:cart_id' => "checkout#place_order"
    end
  end
end
