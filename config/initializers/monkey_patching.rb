class String
  def is_i?
    self.to_i.to_s == self
  end
end

class Hash
  def hash_revert
    r = Hash.new {|h,k| h[k] = []}
    each {|k,v| r[v] << k}
    r
  end

  def reject_non_number_strings!
    self.reject! do |_, v|
      !v.is_i? if v.is_a?(String)
    end
  end
end
