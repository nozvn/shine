[CoreFilters, UrlFilters, PaginateFilters].each do |filters|
  Liquid::Template.register_filter filters
end

Liquid::Template.register_tag 'paginate', PaginateTag
Liquid::Template.register_tag 'gridrow' , GridRowTag

module Liquid
  class Include
    def render_with_file_system(context)
      design = context['shop'].instance_variable_get('@design')
      context.registers[:file_system] = Liquid::SnippetFileSystem.new(File.join(design.path, 'snippets'))
      render_without_file_system(context)
    end

    alias_method_chain :render, :file_system
  end

  class SnippetFileSystem < LocalFileSystem

    def full_path(template_path)
      raise FileSystemError, "Illegal template name '#{template_path}'" unless template_path =~ /^[^.\/][a-zA-Z0-9_\/\-]+$/

      if template_path.include?('/')
        full_path = File.join(root, File.dirname(template_path), "#{File.basename(template_path)}.liquid")
      else
        full_path = File.join(root, "#{template_path}.liquid")
      end

      raise FileSystemError, "Illegal template path '#{File.expand_path(full_path)}'" unless File.expand_path(full_path) =~ /^#{File.expand_path(root)}/

      full_path
    end
  end
end
