Apartment.configure do |config|
  config.tenant_names  = lambda { Shop.pluck(:subdomain) }
  config.excluded_models = %w{User Shop Design Doorkeeper::Application Doorkeeper::AccessGrant Doorkeeper::AccessToken}
end
