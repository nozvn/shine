module ShopFrontend
  module SitemapsHelper
    def sitemap_item_link(resources)
      "http://#{@shop.domain}/sitemap_#{resources}.xml"
    end
  end
end
