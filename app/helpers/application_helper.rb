require_dependency "mobile_detection"

module ApplicationHelper
  def title(text)
    content_for(:page_title) { "APP | #{text}" }
  end

  def html_classes
    "#{mobile_view? ? 'mobile-view' : 'desktop-view'} #{mobile_device? ? 'mobile-device' : 'not-mobile-device'}"
  end

  private

  def mobile_view?
    MobileDetection.resolve_mobile_view!(request.user_agent,params,session)
  end

  def mobile_device?
    MobileDetection.mobile_device?(request.user_agent)
  end
end
