# coding: utf-8
module ProductsHelper
  def currency(price)
    number_to_currency(price, unit: "VNĐ", format: "%n %u", delimiter: ".",
                       precision: (price.round == price) ? 0 : 3)
  end
end
