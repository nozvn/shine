# == Schema Information
#
# Table name: products
#
#  id          :integer          not null, primary key
#  name        :string
#  slug        :string
#  state       :string
#  option1     :string
#  option2     :string
#  option3     :string
#  description :string
#  body        :text
#  deleted_at  :datetime
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Product < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

  acts_as_paranoid

  before_validation :delegate_set_master_attributes
  after_save :save_master!

  has_one :master,
    -> { where is_master: true },
    class_name: 'Variant', inverse_of: :product
  delegate :price, to: :master

  has_many :variants,
    -> { where is_master: false }, inverse_of: :product

  accepts_nested_attributes_for :variants,
    allow_destroy: true,
    reject_if: :no_option_values

  has_many :categorizations
  has_many :categories, through: :categorizations

  validates :name, presence: true
  validates :slug, presence: true, uniqueness: true
  validates :price, numericality: { greater_than: 0 }

  def grouped_option_values
    grouped = {}
    %w{option1 option2 option3}.map do |opt|
      grouped.merge!(
        { eval(opt) => variants.sort_by(&:id).map(&:"#{opt}_value").uniq.reject(&:blank?) }
      ) unless self.send(opt).blank?
    end
    grouped
  end

  def no_option_values(attributed)
    attributed['option1_value'].blank? &&
    attributed['option2_value'].blank? &&
    attributed['option3_value'].blank?
  end

  def price=(num)
    build_master unless master
    master.price = num
  end

  private

  def delegate_set_master_attributes
    build_master unless master
    master.price = price
  end

  def save_master!
    master.save!
  end
end
