# == Schema Information
#
# Table name: addresses
#
#  id         :integer          not null, primary key
#  first_name :string
#  last_name  :string
#  address1   :string
#  address2   :string
#  phone      :string
#  city       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Address < ActiveRecord::Base
  validates :first_name , presence: true
  validates :last_name  , presence: true
  validates :address1   , presence: true
  validates :city       , presence: true
  validates :phone      , presence: true
end
