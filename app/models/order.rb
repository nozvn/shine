# == Schema Information
#
# Table name: orders
#
#  id                  :integer          not null, primary key
#  number              :string
#  email               :string
#  ip_address          :string
#  billing_address_id  :integer
#  shipping_address_id :integer
#  completed_at        :datetime
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class Order < ActiveRecord::Base
  include ActsAsOrder

  belongs_to :billing_address,  class_name: 'Address'
  belongs_to :shipping_address, class_name: 'Address'

  delegate :first_name, to: :billing_address, allow_nil: true
  delegate :last_name , to: :billing_address, allow_nil: true

  has_many :items, class_name: 'OrderItem', dependent: :destroy

  accepts_nested_attributes_for :billing_address, :shipping_address, :items

  validates :email, presence: true

  before_validation :generate_number, on: :create
  before_validation :clone_billing_address, if: :use_billing?
  attr_accessor :use_billing

  def generate_number
    record = true
    while record
      pre    = Array.new(4){(65 + rand(26)).chr}.join
      random = "#{pre}-#{Array.new(6){rand(9)}.join}"
      record = self.class.find_by(number: random)
    end
    self.number = random if self.number.blank?
    self.number
  end

  def clone_billing_address
    if billing_address and shipping_address.nil?
      self.shipping_address = billing_address.dup
    else
      self.shipping_address.attributes = billing_address.attributes.except('id', 'updated_at', 'created_at')
    end
    true
  end

  def save_from_cart(cart)
    transaction do
      self.items_attributes = cart.items.map do |item|
        inventory = InventoryHandler.new
        variant = item.variant

        inventory.take!(variant, item.quantity) if variant.track_inventory

        { variant_id: variant.id, quantity: item.quantity }
      end
      save!
    end
  end

  private

  def use_billing?
    @use_billing == true || @use_billing == 'true' || @use_billing == '1'
  end
end
