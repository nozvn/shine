# == Schema Information
#
# Table name: design_settings
#
#  id         :integer          not null, primary key
#  design_id  :integer
#  name       :string
#  value      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class DesignSetting < ActiveRecord::Base
  belongs_to :design

  validates :name, presence: true, uniqueness: true
  validates :value, presence: true
end
