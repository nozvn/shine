# == Schema Information
#
# Table name: categories
#
#  id          :integer          not null, primary key
#  name        :string
#  slug        :string
#  description :string
#  parent_id   :integer
#  lft         :integer
#  rgt         :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Category < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :categorizations
  has_many :products, through: :categorizations

  acts_as_nested_set

  validates :name, presence: true
  validates :slug, presence: true, uniqueness: true
end
