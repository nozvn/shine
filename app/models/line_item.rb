class LineItem
  include ActsAsOrderItem

  def initialize(variant, quantity)
    @variant = variant
    @quantity = quantity
  end

  attr_reader :variant, :quantity
end
