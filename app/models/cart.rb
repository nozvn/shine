class Cart
  include ActsAsOrder

  def initialize(cart_id)
    @redis_key = get_redis_key(cart_id)
    @hash = get_hash
    @variants = Variant.available.where(id: @hash.keys)
  end

  attr_reader :redis_key

  def add_item(variant_id, quantity)
    quantity = get_item_quantity(variant_id) + quantity
    save_item variant_id, quantity
  end

  def remove_item(variant_id)
    $redis.hdel @redis_key, variant_id
  end

  def update_quantities(params)
    params = convert_params(params)
    params.each do |variant_id, quantity|
      save_item variant_id, quantity
    end
  end

  def clear
    $redis.del @redis_key
  end
  alias :discard :clear

  def items
    @items ||= @variants.map do |variant|
      LineItem.new(variant, @hash[variant.id.to_s])
    end
  end

  def insufficient_stock_lines
    inventory = InventoryHandler.new
    items.select do |item|
      if item.variant.track_inventory
         !inventory.sufficient_stock_level?(item.variant, item.quantity)
      end
    end
  end

  private

  def get_redis_key(cart_id)
    "cart:#{cart_id}"
  end

  def get_hash
    redis_hash = $redis.hgetall(redis_key)
    redis_hash.inject({}) do |cart_hash, (variant_id, quantity)|
      cart_hash[variant_id] = quantity.to_i
      cart_hash
    end
  end

  def get_item_quantity(variant_id)
    $redis.hget(@redis_key, variant_id).to_i || 0
  end

  def save_item(variant_id, quantity)
    if quantity.zero?
      remove_item variant_id
    else
      $redis.hset @redis_key, variant_id, quantity
      $redis.expire @redis_key, 1209600 # 2 weeks
    end
  end

  def convert_params(hash)
    hash.reject_non_number_strings!
    hash.inject({}) do |result, (k, v)|
      result[k.to_i] = v.to_i
      result
    end
  end
end
