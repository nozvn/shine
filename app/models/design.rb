# == Schema Information
#
# Table name: designs
#
#  id          :integer          not null, primary key
#  shop_id     :integer
#  name        :string
#  slug        :string
#  description :text
#  author      :string
#  email       :string
#  website     :string
#  version     :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Design < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

  belongs_to :shop

  has_many :settings, class_name: 'DesignSetting', dependent: :destroy

  validates :name, presence: true
  validates :slug, presence: true, uniqueness: true

  def path
    Rails.root.join('data', 'designs', id.to_s)
  end

  def template_path(name)
    path.join('templates', "#{name}.liquid")
  end

  def layout_path
    template_path('layout')
  end

  def asset_path(name)
    liquid_path = path.join('assets', "#{name}.liquid")
    return liquid_path if File.exist?(liquid_path)
    path.join('assets', name)
  end
end
