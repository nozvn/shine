# == Schema Information
#
# Table name: variants
#
#  id            :integer          not null, primary key
#  product_id    :integer
#  price         :integer
#  is_master     :boolean          default(FALSE)
#  option1_value :string
#  option2_value :string
#  option3_value :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Variant < ActiveRecord::Base
  belongs_to :product
  before_save :ensure_price_is_set

  scope :available, ->() { joins(:product) }

  def self.find_by_id_or_options(params)
    return find(params[:variant_id]) if params[:variant_id]

    condition_parts = ["product_id = '#{params[:product_id]}'"]
    params[:options].each_with_index do |opt, i|
      condition_parts.push "option#{i+1}_value = '#{opt}'"
    end
    condition = condition_parts.join(' AND ')
    where(condition).take
  end

  def ensure_price_is_set
    self.price = product.price unless price
  end

  def name
    product ? product.name : I18n.t('variant.name.unavailable')
  end

  def option_values
    [option1_value, option2_value, option3_value].delete_if(&:blank?)
  end

  def options_string
    option_values.join(' , ')
  end
end
