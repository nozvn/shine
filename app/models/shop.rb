# == Schema Information
#
# Table name: public.shops
#
#  id                 :integer          not null, primary key
#  owner_id           :integer
#  title              :string
#  subdomain          :string
#  selected_design_id :integer          default(1)
#  is_offline         :boolean          default(FALSE)
#  offline_message    :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

require_dependency "reserved_subdomain"

class Shop < ActiveRecord::Base
  belongs_to :owner, class_name: 'User', foreign_key: 'owner_id'
  has_many :designs

  validates :title, presence: true

  validates :subdomain, presence: true,
            uniqueness: true, exclusion: { in: ReservedSubdomain.all }

  after_create :create_schema

  def selected_design
    Design.find(selected_design_id)
  end

  def create_schema
    Apartment::Tenant.create(subdomain)
  end

  def domain
    subdomain + '.lvh.me:5000'
  end
end
