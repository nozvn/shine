module ActsAsOrderItem
  %w{name price options_string}.each do |method_name|
    define_method(method_name) do
      variant.send(method_name)
    end
  end

  def variant_id
    variant.id
  end

  def line_total
    price * quantity
  end
end
