module ActsAsOrder
  def items_count
    return 0 if items.blank?
    items.map(&:quantity).inject(:+)
  end

  def empty?
    items_count == 0
  end

  def total
    items.map(&:line_total).inject(:+)
  end
end
