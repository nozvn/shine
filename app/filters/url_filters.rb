module UrlFilters
  def asset_url(name)
    shop = @context['shop']
    shop.asset_url(name)
  end
end
