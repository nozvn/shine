# encoding: utf-8

module CoreFilters
  include ActionView::Helpers::NumberHelper

  def currency(price)
      number_to_currency price, unit: "VNĐ", format: "%n %u", delimiter: "." ,
        precision: (price.round == price) ? 0 : 3
  end
end
