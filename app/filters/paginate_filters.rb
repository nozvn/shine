# encoding: utf-8

module PaginateFilters
  def default_pagination(paginate)
    current_page = paginate['current_page']
    total_pages = paginate['total_pages']
    result = ""
    if total_pages > 1
      if paginate['previous']
        result += %Q(<span class="prev"><a href="#{paginate['previous']['query']}">« #{paginate['previous']['text']}</a></span>)
      end
      paginate['pages'].each do |page|
        page_number = page['text']
        if page_number == (current_page + 3) && page_number != total_pages
          result += %Q(<span class="gap">...</span>)
        elsif page_number > (current_page + 3) && page_number != total_pages
        elsif page_number == (current_page - 3) && page_number != 1
          result += %Q(<span class="gap">...</span>)
        elsif page_number < (current_page - 3) && page_number != 1
        else
          if page_number == current_page
            result += %Q(<span class="current paged">#{page_number}</span>)
          else
            result += %Q(<span class="paged"><a href="#{page['query']}">#{page_number}</a></span>)
          end
        end
      end
      if paginate['next']
        result += %Q(<span class="next"><a href="#{paginate['next']['query']}">#{paginate['next']['text']} »</a></span>)
      end
    end
    result
  end
end
