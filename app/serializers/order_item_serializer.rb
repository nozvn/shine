class OrderItemSerializer < ActiveModel::Serializer
  attributes :id,
             :price,
             :quantity,
             :options_string

  has_one :variant, embed: :id
end
