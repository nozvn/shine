class ProductSerializer < ActiveModel::Serializer
  attributes :id,
             :name,
             :slug,
             :price,
             :description,
             :body,
             :option1,
             :option2,
             :option3

  has_many :variants
  has_many :categories
end
