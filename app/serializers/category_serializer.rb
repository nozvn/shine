class CategorySerializer < ActiveModel::Serializer
  attributes :id,
             :name,
             :slug,
             :description

  has_one  :parent   , embed: :ids
  has_many :children , embed: :ids
end
