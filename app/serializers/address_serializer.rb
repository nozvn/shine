class AddressSerializer < ActiveModel::Serializer
  attributes :id,
             :first_name,
             :last_name,
             :address1,
             :address2,
             :phone,
             :city
end
