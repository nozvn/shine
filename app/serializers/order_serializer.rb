class OrderSerializer < ActiveModel::Serializer
  attributes :id,
             :number,
             :email,
             :ip_address,
             :completed_at,
             :total

  has_one  :billing_address
  has_one  :shipping_address
  has_many :items
end
