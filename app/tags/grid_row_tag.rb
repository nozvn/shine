class GridRowTag < Liquid::TableRow

  def render(context)
    collection = context[@collection_name] or return ''

    from = @attributes['offset'] ? context[@attributes['offset']].to_i : 0
    to = @attributes['limit'] ? from + context[@attributes['limit']].to_i : nil

    collection = Liquid::Utils.slice_collection_using_each(collection, from, to)

    length = collection.length

    cols = context[@attributes['cols']].to_i

    row = 1
    col = 0

    result = "<div class=\"grid-row row-1\">\n"
    context.stack do

      collection.each_with_index do |item, index|
        context[@variable_name] = item
        context['tablerowloop'] = {
          'length'  => length,
          'index'   => index + 1,
          'col'     => col + 1,
          'col0'    => col,
          'index0'  => index,
          'rindex'  => length - index,
          'rindex0' => length - index - 1,
          'first'   => (index == 0),
          'last'    => (index == length - 1),
          'col_first' => (col == 0),
          'col_last'  => (col == cols - 1)
        }

        col += 1

        result << "<div class=\"grid-col col-#{col}\">" << render_all(@nodelist, context) << '</div>'

        if col == cols and (index != length - 1)
          col  = 0
          row += 1
          result << "</div>\n<div class=\"grid-row row-#{row}\">"
        end

      end
    end
    result << "</div>\n"
    result
  end
end
