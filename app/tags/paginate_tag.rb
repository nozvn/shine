class PaginateTag < Liquid::Block

  Syntax = /(#{Liquid::VariableSignature}+)\s+by\s+(\d+|#{Liquid::VariableSignature}+)/

  def initialize(tag_name, markup, tokens)
    if markup =~ Syntax
      @collection_token = $1
      @per_page_token = $2
    else
      raise Liquid::SyntaxError.new("Syntax Error in 'paginate' - Valid syntax: paginate [collection] by [per_page]")
    end
    super
  end

  def render(context)
    collection = context[@collection_token] or return ""
    size = collection.size
    per_page = get_per_page(context, @per_page_token)
    total_pages = (size + per_page - 1) / per_page
    current_page = (context['current_page'] || 1).to_i
    current_items = collection[(current_page - 1) * per_page, per_page] or return ""
    collection.reject! { |item| !current_items.include?(item) }

    context.stack do
      context['paginate'] = {
        'current_page'   => current_page,
        'per_page'       => per_page,
        'current_offset' => (current_page - 1) * per_page,
        'size'           => size,
        'total_pages'    => total_pages,
      }
      if current_page > 1
        context['paginate']['previous'] = {
          'query' => build_query(context, current_page - 1),
          'text'  => I18n.t('paginate.prev')
        }
      end
      context['paginate']['pages'] = (1..total_pages).map do |page_number|
        {
          'query' => build_query(context, page_number),
          'text'  => page_number
        }
      end
      if current_page < total_pages
        context['paginate']['next'] = {
          'query' => build_query(context, current_page + 1),
          'text'  => I18n.t('paginate.next')
        }
      end
      super
    end
  end

  private

  def build_query(context, current_page)
    query = "?page=#{current_page}"
    if per_page = context['per_page']
      query += "&per_page=#{per_page}"
    end
    query
  end

  def get_per_page(context, token)
    ((token =~ /^\d+$/ && token) || context[token]).to_i
  end
end
