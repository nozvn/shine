scaffold = ->
  $(document).foundation()
  # Hack. Refer to: https://github.com/zurb/foundation/issues/2902
  $(document).foundation 'abide', init: false

$(document).ready scaffold
$(document).on 'page:load', scaffold
