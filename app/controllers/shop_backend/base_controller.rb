class ShopBackend::BaseController < ActionController::Base
  layout 'backend'

  def index

  end

  private

  def bundle_js
    if Rails.env.production?
      'bundle'
    else
      'http://localhost:3000/static/bundle.js'
    end
  end
  helper_method :bundle_js
end
