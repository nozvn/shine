class Api::V1::OrdersController < Api::V1::BaseController
  # before_action :doorkeeper_authorize!
  # before_action :authorize_resource_owner!, except: [:index, :show]

  inherit_resources
  respond_to :json

  private

  def permitted_params
    params.permit(
      order: [
        :email,
        :ip_address,
        :completed_at,
        :billing_address_attributes,
        :shipping_address_attributes
      ]
    )
  end
end
