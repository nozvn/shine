class Api::V1::ProductsController < Api::V1::BaseController
  # before_action :doorkeeper_authorize!
  # before_action :authorize_resource_owner!, except: [:index, :show]

  inherit_resources
  respond_to :json

  def update
    update! do |success, error|
      success.json { render json: resource }
    end
  end

  def destroy
    destroy! do |success, error|
      success.json { render json: { id: resource.id, deleted_at: resource.deleted_at } }
    end
  end

  private

  def permitted_params
    permitted = { product: product_params }

    category_ids = []

    if product_params.key?(:categories)
      category_ids = product_params[:categories]
      permitted[:product].delete(:categories)
    end

    permitted[:product].merge!(
      category_ids: category_ids
    )

    variants_attributes = []

    if product_params.key?(:variants)
      variants_attributes = product_params[:variants].each do |v|
        v[:option1_value] ||= v.delete(:option_1value)
        v[:option2_value] ||= v.delete(:option_2value)
        v[:option3_value] ||= v.delete(:option_3value)
      end
      permitted[:product].delete(:variants)
    end

    permitted[:product].merge!(
      variants_attributes: variants_attributes
    )

    permitted
  end

  def product_params
    params.permit(
        :name,
        :slug,
        :price,
        :description,
        :body,
        :option1, :option2, :option3,
        :categories => [],
        :variants => [
          :id,
          :price,
          :option1_value, :option_1value,
          :option2_value, :option_2value,
          :option3_value, :option_3value,
          :in_stock,
          :track_inventory,
          :_destroy
        ]
    )
  end
end
