module Api
  module V1

    class CategoriesController < Api::V1::BaseController
      # before_action :doorkeeper_authorize!
      # before_action :authorize_resource_owner!, except: [:index, :show]

      inherit_resources
      respond_to :json

      private

      def permitted_params
        params.permit(
          category: [
            :name,
            :slug,
            :description,
            :parent_id,
            :child_ids => [],
            :product_ids => []
          ]
        )
      end
    end

  end
end
