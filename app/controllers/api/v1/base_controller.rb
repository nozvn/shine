module Api
  module V1
    class BaseController < ActionController::Base
      before_action :switch_database

      private

      def authorize_resource_owner!
        render nothing: true, status: 401 if !shop_owner?
      end

      def shop_owner?
        current_resource_owner == shop.owner
      end

      def current_resource_owner
        if doorkeeper_token
          User.find(doorkeeper_token.resource_owner_id)
        else
          current_user
        end
      end

      def shop
        @shop ||= Shop.find_by_subdomain(request.subdomain)
      end

      def switch_database
        Apartment::Tenant.switch!(shop.subdomain)
      end

      def default_serializer_options
        { root: false }
      end
    end
  end
end
