class ShopFrontend::CategoriesController < ShopFrontend::BaseController
  def show
    category = Category.friendly.find(params[:id])

    assigns = {
      'category' => CategoryDrop.new(category),
      'current_page' => params[:page],
      'per_page' => params[:per_page]
    }

    render_liquid_view 'category', assigns
  end
end
