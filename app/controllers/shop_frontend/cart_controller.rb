class ShopFrontend::CartController < ShopFrontend::BaseController
  def show
    render_liquid_view 'cart'
  end

  def update
    current_cart.update_quantities(params[:quantities])
    redirect_to cart_path
  end

  def empty
    current_cart.clear
    redirect_to '/'
  end

  def add_item
    variant = Variant.find_by_id_or_options(params)
    redirect_to :back and return unless variant

    quantity = params[:quantity] ? params[:quantity].to_i : 1
    inventory = InventoryHandler.new

    begin
      inventory.check!(variant, quantity) if variant.track_inventory

      current_cart.add_item(variant.id, quantity)
      redirect_to cart_path

    rescue InsufficientStock
      redirect_to :back
    end
  end

  def remove_item
    current_cart.remove_item(params[:variant_id])
    redirect_to cart_path
  end
end
