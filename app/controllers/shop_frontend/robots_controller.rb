class ShopFrontend::RobotsController < ShopFrontend::BaseController
  skip_before_filter :render_offline_if_set
  skip_before_filter :ensure_new_cart_id

  def index
  end
end
