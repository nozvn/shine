class ShopFrontend::BaseController < ActionController::Base
  before_filter :load_shop
  before_filter :load_design
  before_filter :switch_database
  before_filter :render_offline_if_set
  before_filter :ensure_new_cart_id

  layout nil

  private

  def render_liquid_view(outlet_name, assigns = {})
    layout_markup = File.read(@design.layout_path)
    assigns = merge_liquid_globals(assigns)
    assigns['page_title'] = get_page_title(outlet_name, assigns)
    assigns['outlet'] = render_liquid_outlet(outlet_name, assigns)
    render text: liquidize(layout_markup, assigns)
  end

  def get_page_title(outlet_name, assigns)
    case outlet_name
    when 'cart'      ; t('page_titles.shop.cart')
    when 'home'     ; t('page_titles.shop.home')
    when 'product'   ; assigns['product'].name
    when 'category'  ; assigns['category'].name
    else             ; ""
    end
  end

  def render_liquid_outlet(name, assigns)
    markup = File.read(@design.template_path(name))
    liquidize(markup, assigns)
  end

  def liquidize(markup, assigns)
    Liquid::Template.parse(markup).render(assigns)
  end

  def merge_liquid_globals(assigns)
    assigns.merge({
      'shop'         => ShopDrop.new(@shop),
      'cart'         => CartDrop.new(current_cart),
      'categories'   => CategoriesDrop.new,
      'settings'     => SettingsDrop.new(@design),

      'checkout_url' => get_checkout_url,
      'current_path' => request.path
    })
  end

  def get_checkout_url
    checkout_url shop_id: @shop.id, cart_id: cart_id,
      subdomain: 'checkout'
  end

  def current_cart
    @current_cart ||= Cart.new(cart_id)
  end

  def cart_id
    params[:cart_id] || (session[:cart_id] ||= $redis.incr('cart'))
  end

  def load_shop
    if id = params[:shop_id]
      @shop = Shop.find(id)
    else
      @shop = Shop.find_by_subdomain!(request.subdomain)
    end
  end

  def load_design
    if id = params[:design_id]
      @design = @shop.designs.find(id)
    else
      @design = @shop.selected_design
    end
  end

  def switch_database
    Apartment::Tenant.switch!(@shop.subdomain)
  end

  # clear the session cart key on successful order to make sure we
  # have a new one generated for next order
  def ensure_new_cart_id
    session[:cart_id] = nil if params[:successful_order]
  end

  def render_offline_if_set
    render_liquid_view 'offline' and return if @shop.is_offline
  end
end
