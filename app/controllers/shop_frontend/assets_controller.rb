class ShopFrontend::AssetsController < ShopFrontend::BaseController
  skip_before_filter :render_offline_if_set
  skip_before_filter :ensure_new_cart_id

  def show
    file_name = [params[:name], params[:format]].join('.')
    content = File.read(@design.asset_path(file_name))

    assigns = {
      'shop'     => ShopDrop.new(@shop),
      'settings' => SettingsDrop.new(@design),
    }

    if params[:format] =~ /^(css|js)$/
      content = liquidize(content, assigns)
    end
    render text: content
  end
end
