class ShopFrontend::CheckoutController < ShopFrontend::BaseController
  before_filter :load_cart
  before_filter :redirect_if_empty_cart

  protect_from_forgery with: :exception

  def index
    @order = Order.new
    @order.billing_address  ||= Address.new
    @order.shipping_address ||= Address.new
  end

  def place_order
    @order = Order.new(permitted_params)

    if Checkout.new(@cart, request.remote_ip).place_order(@order)
      redirect_to home_url(subdomain: @shop.subdomain, successful_order: true)
    else
      render :index
    end
  rescue InsufficientStock
    redirect_to cart_url(subdomain: @shop.subdomain)
  end

  private

  def load_cart
    @cart = current_cart
  end

  def redirect_if_empty_cart
    if @cart.empty?
      redirect_to cart_url(subdomain: @shop.subdomain)
    end
  end

  def permitted_params
    params[:order].permit!
  end
end
