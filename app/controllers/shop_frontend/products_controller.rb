class ShopFrontend::ProductsController < ShopFrontend::BaseController
  def show
    product = Product.friendly.find(params[:id])
    render_liquid_view 'product', 'product' => ProductDrop.new(product)
  end
end
