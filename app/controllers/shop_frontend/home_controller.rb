class ShopFrontend::HomeController < ShopFrontend::BaseController
  def index
    render_liquid_view 'home'
  end
end
