class Checkout
  def initialize(cart, ip_address)
    @cart = cart
    @ip_address = ip_address
  end

  def place_order(order)
    order.ip_address = @ip_address
    order.save_from_cart(@cart)
    @cart.discard
  end
end
