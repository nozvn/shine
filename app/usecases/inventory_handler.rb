class InventoryHandler
  def sufficient_stock_level?(variant, quantity)
    quantity <= variant.in_stock || variant.allow_backorder?
  end

  def check!(variant, quantity)
    if !sufficient_stock_level?(variant, quantity)
      raise InsufficientStock
    end
  end

  def take!(variant, quantity)
    check!(variant, quantity)
    variant.update_attributes(in_stock: variant.in_stock - quantity)
  end
end

class InsufficientStock < Exception; end
