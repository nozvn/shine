xml.urlset(xmlns: "http://www.sitemaps.org/schemas/sitemap/0.9") do
  xml.url do
    xml.loc "http://#{@shop.domain}"
    xml.changefreq("daily")
  end

  @products.each do |product|
    xml.url do
      xml.loc product_url(product, domain: @shop.domain)
      xml.lastmod product.updated_at.xmlschema
      xml.changefreq("daily")
    end
  end
end
