xml.sitemapindex(xmlns: "http://www.sitemaps.org/schemas/sitemap/0.9") do
  xml.sitemap do
    xml.loc sitemap_item_link(:categories)
  end

  xml.sitemap do
    xml.loc sitemap_item_link(:products)
  end
end
