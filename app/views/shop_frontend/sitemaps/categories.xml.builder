xml.urlset(xmlns: "http://www.sitemaps.org/schemas/sitemap/0.9") do
  @categories.each do |category|
    xml.url do
      xml.loc category_url(category, domain: @shop.domain)
      xml.lastmod category.updated_at.xmlschema
      xml.changefreq("daily")
    end
  end
end
