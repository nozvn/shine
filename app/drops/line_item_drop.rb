class LineItemDrop < Liquid::Drop
  def initialize(line_item)
    @line_item = line_item
  end

  delegate :variant_id, :name, :options_string, :price, :quantity, :line_total,
    to: :@line_item, allow_nil: true
end
