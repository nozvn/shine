class CartDrop < Liquid::Drop
  def initialize(cart)
    @cart = cart
  end

  delegate :items_count, :total, to: :@cart, allow_nil: true

  def items
    @items ||= @cart.items.map do |item|
      LineItemDrop.new(item)
    end
  end
end
