class SettingsDrop < Liquid::Drop
  def initialize(design)
    @design = design
  end

  def before_method(name)
    @design.settings.where(name: name).take.try(:value)
  end
end
