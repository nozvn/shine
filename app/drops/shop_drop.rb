class ShopDrop < Liquid::Drop
  def initialize(shop)
    @shop = shop
    @design = shop.selected_design
  end

  delegate :id, :title, :subdomain, :offline_message, to: :@shop, allow_nil: true

  def asset_url(name)
    File.join('/data', @shop.id.to_s, 'designs', @design.id.to_s, 'assets', name)
  end
end
