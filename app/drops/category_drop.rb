class CategoryDrop < Liquid::Drop
  def initialize(category)
    @category = category
  end

  delegate :id, :name, :slug, :description, to: :@category, allow_nil: true

  def path
    "/categories/#{slug}"
  end

  def products
    @products ||= @category.products.includes(:master).map do |product|
      ProductDrop.new(product)
    end
  end

  def products_count
    products.count
  end
end
