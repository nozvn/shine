class CategoriesDrop < Liquid::Drop
  def initialize
    @categories = Category.all
  end

  def before_method(slug)
    category = @categories.find_by_slug(slug) || Category.new
    CategoryDrop.new(category)
  end

  def all
    @all ||= @categories.map do |category|
      CategoryDrop.new(category)
    end
  end

  def roots
    @roots ||= Category.roots.map do |category|
      CategoryDrop.new(category)
    end
  end
end
