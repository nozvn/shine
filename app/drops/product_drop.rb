class ProductDrop < Liquid::Drop
  def initialize(product)
    @product = product
  end

  delegate :id, :name, :slug, :price, :description, :body,
    to: :@product, allow_nil: true

  alias :sale_price :price

  def path
    "/products/#{slug}"
  end

  def master_id
    @product.master.id
  end

  def variants_count
    @product.variants.count
  end

  def options
    @product.grouped_option_values.map do |group, values|
      ProductOptionDrop.new(group, values)
    end
  end
end

class ProductOptionDrop < Liquid::Drop
  def initialize(group, values)
    @group  = group
    @values = values
  end

  attr_reader :group, :values
end
