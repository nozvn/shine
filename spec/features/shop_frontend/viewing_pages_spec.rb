require "rails_helper"

feature "Viewing pages", %q{
  In order to check out this online store
  As a visitor
  I want to be able to view its web pages
} do

  given!(:shop) { setup_shop(title: 'Chess World', subdomain: 'chessworld') }

  background do
    switch_to_subdomain shop.subdomain
  end

  scenario "view front page" do
    visit '/'
    expect(page).to have_text('Chess World')
  end
end
