# coding: utf-8
require "rails_helper"

feature "Browsing products", %q{
  In order to shop
  As a customer
  I want to be able to browse through products
} do

  given!(:shop) { setup_shop(subdomain: 'tvworld') }

  background do
    switch_to_subdomain('tvworld')
    Apartment::Tenant.switch!('tvworld')
  end

  scenario "view paginated list of categorized products" do
    category = create(:category_with_products, name: 'Accessories', products_count: 3)

    visit category_path(category, per_page: 2)

    expect(page).to have_content('Accessories')
    expect(page).to have_css('.product-wrapper', count: 2)
  end

  scenario "view product details" do
    product_body = <<-END
      We'll also unbox it, put it on the stand, plug it in - at no extra cost.
    END
    product = create(:product, name: 'Samsung TV', price: 5030000, body: product_body)

    visit product_path(product)

    expect(page).to have_text('Samsung TV')
    expect(page).to have_text('5.030.000 VNĐ')
    expect(page).to have_text(product_body)
  end
end
