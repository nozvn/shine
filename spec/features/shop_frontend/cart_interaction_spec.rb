# coding: utf-8
require "rails_helper"

feature "Interacting with cart", %q{
  In order to make an order
  As a customer
  I want to add products to cart and checkout
} do

  given!(:shop) { setup_shop(subdomain: 'tvworld') }

  background do
    switch_to_subdomain('tvworld')
    Apartment::Tenant.switch!('tvworld')
  end

  scenario "add an item to cart" do
    add_an_item_to_cart
    expect_non_empty_cart
  end

  scenario "add an item with options to cart" do
    product = create(:product, option1: 'size', option2: 'color')
    create(:variant, option1_value: 'xs', option2_value: 'blue', product: product)
    create(:variant, option1_value: 's', option2_value: 'blue', product: product)

    visit product_path(product)
    select 's', from: 'size-options'
    select 'blue', from: 'color-options'
    click_button 'add-to-cart'

    expect_non_empty_cart
  end

  scenario "add an item with unavailable options to cart" do
    product = create(:product, option1: 'size', option2: 'color')
    create(:variant, option1_value: 'xs', option2_value: 'blue', product: product)
    create(:variant, option1_value: 's', product: product)

    visit product_path(product)
    select 's', from: 'size-options'
    select 'blue', from: 'color-options'
    click_button 'add-to-cart'

    expect_empty_cart
  end

  scenario "have multiple items in cart" do
    3.times do
      add_an_item_to_cart
    end
    expect_cart_with_items(3)
  end

  scenario "update cart" do
    add_an_item_to_cart(price: 10000)
    fill_in 'qty', with: '96'
    first('button[type="submit"]').click
    expect(page).to have_content('960.000 VNĐ')
  end

  scenario "update cart with invalid quantity value" do
    add_an_item_to_cart(price: 10000)
    fill_in 'qty', with: 'xyz'
    first('button[type="submit"]').click
    expect(page).to have_content('10.000 VNĐ')
  end

  scenario "remove an item from cart" do
    add_an_item_to_cart
    click_link 'X'
    expect_empty_cart
  end

  scenario "remove an item by updating with zero qty" do
    add_an_item_to_cart
    fill_in 'qty', with: '0'
    first('button[type="submit"]').click
    expect_empty_cart
  end

  scenario "discard current cart" do
    add_an_item_to_cart
    click_link 'empty-cart'
    visit cart_path
    expect_empty_cart
  end

  scenario "check out" do
    add_an_item_to_cart
    click_link 'check-out'
    fill_in 'order_billing_address_attributes_first_name' , with: 'John'
    fill_in 'order_billing_address_attributes_last_name'  , with: 'Doe'
    fill_in 'order_billing_address_attributes_address1'   , with: '123 1st St'
    fill_in 'order_billing_address_attributes_city'       , with: 'NYC'
    fill_in 'order_billing_address_attributes_phone'      , with: '234-789-4321'
    fill_in 'order_email'                                 , with: 'doe@example.com'
    first('input[type="submit"]').click

    expect_redir_to_home_of('tvworld', successful_order: true)
  end

  private

  def add_an_item_to_cart(options={}, qty=1)
    product = create(:product, options)
    visit product_path(product)
    click_button 'add-to-cart'
  end

  def expect_cart_with_items(count=1)
    expect(page).to have_css('.line-item', count: count)
  end
  alias :expect_non_empty_cart :expect_cart_with_items

  def expect_empty_cart
    expect(page).to_not have_css('.line-item')
  end
end
