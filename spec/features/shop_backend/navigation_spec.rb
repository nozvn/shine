require "rails_helper"

feature "Navigation" do
  given!(:shop) { setup_shop(subdomain: 'chessworld') }

  background do
    visit backend_root_url(subdomain: 'chessworld')
  end

  scenario "navigates to products list" do
    expect_link_to_index(:products)
  end

  scenario "navigates to categories list" do
    expect_link_to_index(:categories)
  end

  scenario "navigates to orders list" do
    expect_link_to_index(:orders)
  end

  scenario "navigates to orders list" do
    expect_link_to_index(:pages)
  end

  scenario "navigates to settings page" do
    expect_link_to_index(:settings)
  end

  private

  def expect_link_to_index(ctrler)
    within "nav.side-bar" do
      expect(page).to have_css("a[href~='/backend/#{ctrler.to_s}']")
    end
  end
end
