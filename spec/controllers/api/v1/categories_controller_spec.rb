require "rails_helper"

describe Api::V1::CategoriesController do
  let(:token) { double(:acceptable? => true) }
  let(:category_params) { attributes_for(:category, name: 'Accessories') }

  before :each do
    allow(controller).to receive(:doorkeeper_token).and_return(token)
    allow(controller).to receive(:shop_owner?).and_return(true)

    create(:shop, subdomain: "chessworld")

    Apartment::Tenant.switch!('chessworld')
    @category = create(:category)

    @request.host = "chessworld.example.com"
  end

  describe "index" do
    it "lists categories" do
      get :index, format: 'json'

      categories_list = JSON.parse(response.body)['categories']
      expect(categories_list.size).to eq(1)
    end
  end

  describe "create" do
    it "creates new category" do
      expect {
        post :create, category: category_params, format: 'json'
      }.to change{ Category.count }.by(1)
    end
  end

  describe "update" do
    it "updates category" do
      expect {
        patch :update, id: @category.id, category: category_params, format: 'json'
        @category.reload
      }.to change{ @category.name }.to('Accessories')
    end
  end

  describe "destroy" do
    it "destroys category" do
      expect {
        delete :destroy, id: @category.id, format: 'json'
      }.to change{ Category.count }.by(-1)
    end
  end
end
