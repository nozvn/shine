require "rails_helper"

describe Api::V1::OrdersController do
  let(:token) { double(:acceptable? => true) }
  let(:order_params) { attributes_for(:order, email: "johnplayschess@example.com") }

  before :each do
    allow(controller).to receive(:doorkeeper_token).and_return(token)
    allow(controller).to receive(:shop_owner?).and_return(true)

    create(:shop, subdomain: "chessworld")

    Apartment::Tenant.switch!('chessworld')
    @order = create(:order)

    @request.host = "chessworld.example.com"
  end

  describe "index" do
    it "lists orders" do
      get :index, format: 'json'

      orders_list = JSON.parse(response.body)['orders']
      expect(orders_list.size).to eq(1)
    end
  end

  describe "create" do
    it "creates new order" do
      expect {
        post :create, order: order_params, format: 'json'
      }.to change{ Order.count }.by(1)
    end
  end

  describe "update" do
    it "updates order" do
      expect {
        patch :update, id: @order.id, order: order_params, format: 'json'
        @order.reload
      }.to change{ @order.email }.to('johnplayschess@example.com')
    end
  end

  describe "destroy" do
    it "destroys order" do
      expect {
        delete :destroy, id: @order.id, format: 'json'
      }.to change{ Order.count }.by(-1)
    end
  end
end
