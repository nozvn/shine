require "rails_helper"

describe Api::V1::ProductsController do
  let(:token) { double(:acceptable? => true) }
  let(:product_params) { attributes_for(:product, name: 'Superb chess set') }

  before :each do
    allow(controller).to receive(:doorkeeper_token).and_return(token)
    allow(controller).to receive(:shop_owner?).and_return(true)

    create(:shop, subdomain: "chessworld")

    Apartment::Tenant.switch!('chessworld')
    @product = create(:product)

    @request.host = "chessworld.example.com"
  end

  describe "index" do
    it "lists products" do
      get :index, format: 'json'

      products_list = JSON.parse(response.body)['products']
      expect(products_list.size).to eq(1)
    end
  end

  describe "create" do
    it "creates new product" do
      expect {
        post :create, product: product_params, format: 'json'
      }.to change{ Product.count }.by(1)
    end
  end

  describe "update" do
    it "updates product" do
      expect {
        patch :update, id: @product.id, product: product_params, format: 'json'
        @product.reload
      }.to change{ @product.name }.to('Superb chess set')
    end
  end

  describe "destroy" do
    it "destroys product" do
      expect {
        delete :destroy, id: @product.id, format: 'json'
      }.to change{ Product.count }.by(-1)
    end
  end
end
