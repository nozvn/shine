module ShopHelper
  def setup_shop(options)
    shop = create(:shop, options)
    shop.designs << create(:design, id: 1)
    shop
  end

  def categorize(p, c)
    if p.is_a?(Array)
      p.each do |product|
        Categorization.create(product: product, category: c)
      end
    else
        Categorization.create(product: p, category: c)
    end
  end
end

RSpec.configure do |config|
  config.include ShopHelper, type: :feature
end
