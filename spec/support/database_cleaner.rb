RSpec.configure do |config|
  config.before(:suite) do
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before(:each) do
    DatabaseCleaner.strategy = :transaction
  end

  config.before(:each, js: true) do
    DatabaseCleaner.strategy = :truncation
  end

  config.before(:each) do
    Apartment::Tenant.reset
    DatabaseCleaner.start
  end

  config.after(:each) do
    Apartment::Tenant.reset
    DatabaseCleaner.clean

    connection = ActiveRecord::Base.connection.raw_connection

    schemas = connection.query(%Q{
      SELECT 'drop schema ' || nspname || ' cascade;'
      from pg_namespace
      where nspname != 'public'
      AND nspname NOT LIKE 'pg_%'
      AND nspname != 'information_schema';
    })

    schemas.each do |query|
      connection.query(query.values.first)
    end
  end
end
