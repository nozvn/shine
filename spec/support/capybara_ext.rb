module CapybaraExt
  def switch_to_subdomain(subdomain)
    Capybara.app_host = "http://#{subdomain}.lvh.me:#{Capybara.server_port}"
  end

  def expect_redir_to_home_of(subdomain, params)
    url = "http://#{subdomain}.lvh.me:#{Capybara.server_port}"
    url += '/?' + URI.encode_www_form(params) if params
    expect(current_url).to eq(url)
  end
end

RSpec.configure do |config|
  config.include CapybaraExt, type: :feature
end
