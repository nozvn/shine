shared_examples "acts as order item" do |item|
  it "delegates some attributes to variant" do
    allow(item).to receive(:variant).and_return(double(name: 'T-shirt', options_string: 'red , small', price: 150))

    expect(item.name).to eq 'T-shirt'
    expect(item.price).to eq 150
    expect(item.options_string).to eq 'red , small'
  end

  it "calculates line total as product of quantity and price" do
    allow(item).to receive(:variant).and_return(double(price: 190))
    allow(item).to receive(:quantity).and_return(2)
    expect(item.line_total).to eq 380
  end
end
