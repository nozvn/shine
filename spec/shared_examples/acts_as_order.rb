shared_examples "acts as order" do |order|
  it "counts line items" do
    allow(order).to receive(:items).and_return([double(quantity: 3), double(quantity: 2)])

    expect(order.items_count).to eq 5
  end

  it "is empty when items count is zero" do
    allow(order).to receive(:items_count).and_return(0)
    expect(order.empty?).to eq true
  end

  it "is not empty when items count is non-zero" do
    allow(order).to receive(:items_count).and_return(5)
    expect(order.empty?).to eq false
  end

  it "calculates total as sum of line totals" do
    allow(order).to receive(:items).and_return([double(line_total: 200), double(line_total: 150)])
    expect(order.total).to eq 350
  end
end
