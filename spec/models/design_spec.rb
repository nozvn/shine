# == Schema Information
#
# Table name: designs
#
#  id          :integer          not null, primary key
#  shop_id     :integer
#  name        :string
#  slug        :string
#  description :text
#  author      :string
#  email       :string
#  website     :string
#  version     :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

describe Design do
  it { should belong_to(:shop) }
  it { should have_many(:settings).class_name('DesignSetting').dependent(:destroy) }

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:slug) }
  it { should validate_uniqueness_of(:slug) }

  describe "file system" do
    let(:design) { Design.new }

    before do
      allow(design).to receive(:id).and_return(19)
    end

    it "data located at RAILS_ROOT/data/designs/:id" do
      expect(design.path.to_s).to eq(Rails.root.to_s + '/data/designs/19')
    end

    it "liquid files located inside 'templates' sub-folder" do
      expect(design.template_path('front').to_s).to eq(design.path.to_s + '/templates/front.liquid')
    end

    it "layout file is a template file named layout.liquid" do
      expect(design.layout_path.to_s).to eq(design.path.to_s + '/templates/layout.liquid')
    end

    it "asset file located inside 'assets' sub-folder" do
      expect(design.asset_path('main.js').to_s).to eq(design.path.to_s + '/assets/main.js')
    end
  end
end
