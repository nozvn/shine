# == Schema Information
#
# Table name: variants
#
#  id            :integer          not null, primary key
#  product_id    :integer
#  price         :integer
#  is_master     :boolean          default(FALSE)
#  option1_value :string
#  option2_value :string
#  option3_value :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'rails_helper'

describe Variant do
  it { should have_db_column(:is_master).
              of_type(:boolean).
              with_options(default: false) }

  it { should belong_to(:product) }

  it "sets price equals product price if given none" do
    product = create(:product, price: 120)
    variant = product.variants.create(product: product)
    expect(variant.price).to eq 120
  end

  it "presents option values as concatenated string" do
    variant = create(:variant, option1_value: 'red', option2_value: 'small')
    expect(variant.options_string).to eq 'red , small'
  end

  describe "scopes" do
    describe "available" do
      it "selects variants that belongs to an existing product" do
        product = create(:product)
        variant = create(:variant, product: product)
        expect(Variant.available).to include(variant)
      end

      it "does not select variants that don't belong to an existing product" do
        product = create(:product)
        variant = create(:variant, product: product)
        product.destroy
        expect(Variant.available).to_not include(variant)
      end
    end
  end
end
