require 'rails_helper'
require 'shared_examples/acts_as_order_item'

describe LineItem do
  include_examples "acts as order item", LineItem.new
end
