# == Schema Information
#
# Table name: orders
#
#  id                  :integer          not null, primary key
#  number              :string
#  email               :string
#  ip_address          :string
#  billing_address_id  :integer
#  shipping_address_id :integer
#  completed_at        :datetime
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

require 'rails_helper'
require 'shared_examples/acts_as_order'

describe Order do
  include_examples "acts as order", Order.new

  it { should have_many(:items).class_name('OrderItem') }
  it { should accept_nested_attributes_for(:items) }

  it { should belong_to(:billing_address) }
  it { should belong_to(:shipping_address) }
  it { should accept_nested_attributes_for(:billing_address) }
  it { should accept_nested_attributes_for(:shipping_address) }

  it { should validate_presence_of(:email) }

  it "can use billing address as shipping address" do
    order = create(:order, use_billing: true, shipping_address: nil)

    expect(order.shipping_address.address1).to eq(order.billing_address.address1)
  end

  it "delegates first and last name to billing address" do
    order = Order.new
    address = build_stubbed(:address, first_name: "John", last_name: "Doe")
    order.billing_address = address

    expect(order.first_name).to eq "John"
    expect(order.last_name).to eq "Doe"
  end

  it "generates a random number on create" do
    order = create(:order)
    expect(order.number.length).to eq 11
  end
end
