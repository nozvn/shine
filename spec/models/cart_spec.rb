require 'rails_helper'
require 'shared_examples/acts_as_order'

describe Cart do
  include_examples "acts as order", Cart.new(9999)

  describe "insufficient stock lines" do
    it "counts items that has quantity greater than its variant stock level" do
      cart = Cart.new(25)
      variant1 = build_stubbed(:variant)
      variant2 = build_stubbed(:variant_inventory_tracked, in_stock: 1)
      variant3 = build_stubbed(:variant_inventory_tracked, in_stock: 1)
      variant4 = build_stubbed(:variant_inventory_tracked, in_stock: 10)

      allow(cart).to receive(:items).and_return([double(quantity: 99, variant: variant1),
                                                 double(quantity: 1,  variant: variant2),
                                                 double(quantity: 10, variant: variant3),
                                                 double(quantity: 11, variant: variant4)])

      expect(cart.insufficient_stock_lines.size).to eq(2)
    end

    it "does not count items that are allowed to be back-ordered" do
      cart = Cart.new(25)
      variant = build_stubbed(:variant_backorder_allowed, in_stock: 1)
      allow(cart).to receive(:items).and_return([double(quantity: 10, variant: variant)])

      expect(cart.insufficient_stock_lines).to be_empty
    end
  end
end
