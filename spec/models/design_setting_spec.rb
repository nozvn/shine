# == Schema Information
#
# Table name: design_settings
#
#  id         :integer          not null, primary key
#  design_id  :integer
#  name       :string
#  value      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

describe DesignSetting do
  it { should belong_to(:design) }

  it { should validate_presence_of(:name) }
  it { should validate_uniqueness_of(:name) }
  it { should validate_presence_of(:value) }
end
