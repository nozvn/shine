# == Schema Information
#
# Table name: order_items
#
#  id         :integer          not null, primary key
#  order_id   :integer
#  variant_id :integer
#  quantity   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'
require 'shared_examples/acts_as_order_item'

describe OrderItem do
  it { should belong_to(:order) }
  it { should belong_to(:variant) }

  include_examples "acts as order item", OrderItem.new
end
