# == Schema Information
#
# Table name: categorizations
#
#  id          :integer          not null, primary key
#  product_id  :integer
#  category_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

describe Categorization do
  it { should belong_to(:product) }
  it { should belong_to(:category) }
end
