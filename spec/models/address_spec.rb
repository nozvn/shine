# == Schema Information
#
# Table name: addresses
#
#  id         :integer          not null, primary key
#  first_name :string
#  last_name  :string
#  address1   :string
#  address2   :string
#  phone      :string
#  city       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

describe Address do
  it { should validate_presence_of(:first_name) }
  it { should validate_presence_of(:last_name)  }
  it { should validate_presence_of(:address1)   }
  it { should validate_presence_of(:city)       }
  it { should validate_presence_of(:phone)      }
end
