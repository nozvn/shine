# == Schema Information
#
# Table name: products
#
#  id          :integer          not null, primary key
#  name        :string
#  slug        :string
#  state       :string
#  option1     :string
#  option2     :string
#  option3     :string
#  description :string
#  body        :text
#  deleted_at  :datetime
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

describe Product do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:slug) }

  it "requires an unique slug" do
    create(:product, slug: 'tie')
    product = build_stubbed(:product, slug: 'tie')
    expect(product.valid?).to be_falsy
  end

  it "requires a numerical value of price" do
    product = build_stubbed(:product)
    product.price = "some text"
    expect(product.valid?).to be_falsy

    product.price = 19
    expect(product.valid?).to be_truthy
  end

  it { should have_one(:master).class_name('Variant') }

  it "initializes master variant on create" do
    product = create(:product)
    expect(product.master).to be
  end

  it "delegates price attribute to master variant" do
    product = create(:product, price: 90)
    master = product.master
    expect(master.price).to eq 90
    expect(product.price).to eq 90
  end

  it { should have_many(:variants) }
  it { should accept_nested_attributes_for(:variants).allow_destroy(true) }

  it "rejects variant attributes if no option value is given" do
    product = create(:product, variants_attributes: [
      { option1_value: "Blue", option2_value: "Large" },
      { option1_value: ""    , option2_value: "", option3_value: "" }
    ]);
    expect(product.variants.count).to eq 1
  end

  it "groups option values by type" do
    product = create(:product, option1: "Size", option2: "Color", option3: "")

    product.variants.create(price: 93000,
      option1_value: "Small", option2_value: "Red", option3_value: "")

    product.variants.create(price: 95000,
      option1_value: "Medium", option2_value: "", option3_value: "")

    expect(product.grouped_option_values).to eq({ "Size" => ["Small", "Medium"] ,
      "Color" => ["Red"]})
  end

  it { should have_many(:categorizations) }
  it { should have_many(:categories).through(:categorizations) }

  it "auto-generates an unique friendly id" do
    product_1 = create(:product, name: "Coffee Beans")
    expect(product_1.slug).to eq "coffee-beans"

    product_2 = create(:product, name: "Coffee Beans")
    expect(product_2.slug).not_to eq "coffee-beans"
  end

  it "accepts provided slug as its friendly id" do
    product = create(:product, name: "Coffee Mug", slug: "mug")
    expect(product.slug).to eq "mug"
  end

  it "is not be destroyed pernamently" do
    product = create(:product)
    product.destroy
    expect(product).to be_persisted
    expect(product.deleted_at).not_to be_nil
  end
end
