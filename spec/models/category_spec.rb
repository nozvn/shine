# == Schema Information
#
# Table name: categories
#
#  id          :integer          not null, primary key
#  name        :string
#  slug        :string
#  description :string
#  parent_id   :integer
#  lft         :integer
#  rgt         :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

describe Category do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:slug) }
  it { should validate_uniqueness_of(:slug) }

  it { should belong_to(:parent).class_name('Category')}
  it { should have_many(:children).class_name('Category')}

  it { should have_many(:categorizations) }
  it { should have_many(:products).through(:categorizations) }

  describe ".roots" do
    it "returns all categories that do not belong to a parent" do
      fashion = create(:category)

      men = create(:category, parent: fashion)

      expect(Category.roots).to include(fashion)
      expect(Category.roots).not_to include(men)
    end
  end

  describe ".leaves" do
    it "returns all inner-most categories" do
      fashion = create(:category)

      men     = create(:category, parent: fashion)
      women   = create(:category, parent: fashion)

      winter  = create(:category, parent: men)

      expect(Category.leaves).to include(women)
      expect(Category.leaves).to include(winter)
      expect(Category.leaves).not_to include(fashion)
    end
  end

  describe "#siblings" do
    context "when parent has many children" do
      it "returns categories from same parent" do
        fashion = create(:category)
        toys    = create(:category)

        men     = create(:category, parent: fashion)
        women   = create(:category, parent: fashion)

        expect(fashion.siblings.count).to eq(1)
        expect(fashion.siblings).to include(toys)

        expect(fashion.siblings.count).to eq(1)
        expect(men.siblings).to include(women)
      end
    end

    context "when parent has only one child" do
      it "returns empty collection" do
        fashion = create(:category)
        men     = create(:category, parent: fashion)

        expect(fashion.siblings).to be_empty
        expect(men.siblings).to be_empty
      end
    end
  end
end
