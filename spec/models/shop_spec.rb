# == Schema Information
#
# Table name: public.shops
#
#  id                 :integer          not null, primary key
#  owner_id           :integer
#  title              :string
#  subdomain          :string
#  selected_design_id :integer          default(1)
#  is_offline         :boolean          default(FALSE)
#  offline_message    :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

require 'rails_helper'

describe Shop do
  it { should belong_to(:owner).class_name('User') }
  it { should have_many(:designs) }

  it { should validate_presence_of(:title) }

  it { should validate_presence_of(:subdomain) }
  it { should validate_uniqueness_of(:subdomain)}
  it { should validate_exclusion_of(:subdomain).in_array(ReservedSubdomain.all) }

  def schema_exists?(shop)
    query = %Q{SELECT nspname FROM pg_namespace
      WHERE nspname='#{shop.subdomain}'}
    result = ActiveRecord::Base.connection.select_value(query)
    result.present?
  end

  it "has a database schema named same as subdomain" do
    shop = create(:shop)
    failure_message = "Schema #{shop.subdomain} does not exist"
    assert schema_exists?(shop), failure_message
  end

  it "has one selected design" do
    design  = create(:design)
    shop = create(:shop, selected_design_id: design.id)
    expect(shop.selected_design).to eq(design)
  end
end
