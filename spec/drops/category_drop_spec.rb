require "rails_helper"

describe CategoryDrop do
  let(:category)      { create(:category) }
  let(:category_drop) { CategoryDrop.new(category) }

  let(:category_with_product_drop) do
    category.products << create(:product)
    CategoryDrop.new(category)
  end

  it "delegates some attributes to @category" do
    %w[id name slug description].each do |attribute|
      expect(category_drop.send(attribute)).to eq category.send(attribute)
    end
  end

  it "path is /categories/:slug" do
    drop = CategoryDrop.new(build_stubbed(:category, slug: 'clothes'))
    expect(drop.path).to eq('/categories/clothes')
  end

  describe "products" do
    it "#products returns drops of all products in category" do
      product = create(:product)
      Categorization.create(product: product, category: category)
      expect(category_drop.products.first).to be_a(ProductDrop)
    end

    it "#products_count returns number of products in category" do
      expect(category_drop.products_count).to eq(0)
      expect(category_with_product_drop.products_count).to eq(1)
    end
  end
end
