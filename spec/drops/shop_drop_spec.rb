require "rails_helper"

describe ShopDrop do
  let(:design) { create(:design) }
  let(:shop)      { create(:shop, selected_design_id: design.id) }
  let(:shop_drop) { ShopDrop.new(shop)}

  it "delegates some attributes to @shop" do
    %w[id title subdomain offline_message].each do |attribute|
      expect(shop_drop.send(attribute)).to eq shop.send(attribute)
    end
  end
end
