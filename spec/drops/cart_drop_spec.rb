require "rails_helper"

describe CartDrop do
  let(:cart)      { Cart.new(9999) }
  let(:cart_drop) { CartDrop.new(cart) }

  it "delegates some attributes to @cart" do
    allow(cart).to receive(:items_count).and_return(2)
    allow(cart).to receive(:total).and_return(30000)

    expect(cart_drop.items_count).to eq(2)
    expect(cart_drop.total).to eq(30000)
  end

  it "#items returns drops of all line items in cart" do
    allow(cart).to receive(:items).and_return([double, double])
    expect(cart_drop.items.first).to be_a(LineItemDrop)
    expect(cart_drop.items.count).to eq(2)
  end
end
