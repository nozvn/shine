require "rails_helper"

describe ProductDrop do
  let(:product)      { create(:product) }
  let(:product_drop) { ProductDrop.new(product) }

  it "delegates some attributes to @product" do
    %w[id name slug price description body].each do |attribute|
      expect(product_drop.send(attribute)).to eq product.send(attribute)
    end
  end

  it "path is /products/:slug" do
    drop = ProductDrop.new(build_stubbed(:product, slug: 'tshirt'))
    expect(drop.path).to eq('/products/tshirt')
  end

  it "#variants_count returns number of variants" do
    expect(product_drop.variants_count).to eq(0)
    product.variants << create(:variant)
    expect(product_drop.variants_count).to eq(1)
  end

  it "#options returns drops of all product options" do
    product = create(:product, option1: 'size')
    create(:variant, product: product, option1_value: 'large')
    create(:variant, product: product, option1_value: 'x-large')

    product_drop = ProductDrop.new(product)

    markup = <<-END
      {% for option in product.options %}
        {{ option.group }}:{{ option.values.first }}/{{ option.values.last }}
      {% endfor %}
    END


    expect(liquidize(markup, product_drop)).to eq('size:large/x-large')
  end

  private

  def liquidize(markup, drop)
    Liquid::Template.parse(markup).render('product'=>drop).gsub(/\s+/, "")
  end
end
