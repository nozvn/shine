require "rails_helper"

describe LineItemDrop do
  let(:item) do
    item = LineItem.new(double(id: 1), 2) # variant_id, quantity
    allow(item).to receive(:name).and_return('Yellow Tshirt')
    allow(item).to receive(:price).and_return(19000)
    allow(item).to receive(:options_string).and_return('abcxyz')
    item
  end

  let(:item_drop) { LineItemDrop.new(item) }

  it "delegates some attributes to @line_item" do
    expect(item_drop.variant_id).to eq(1)
    expect(item_drop.name).to eq('Yellow Tshirt')
    expect(item_drop.price).to eq(19000)
    expect(item_drop.quantity).to eq(2)
    expect(item_drop.options_string).to eq('abcxyz')
    expect(item_drop.line_total).to eq(38000)
  end
end
