require "rails_helper"

describe CategoriesDrop do
  it "selects category directly by slug" do
    create(:category, slug: 'clothing', description: 'Our clothing products')
    categories_drop = CategoriesDrop.new
    markup = "{{ categories.clothing.description }}"

    expect(liquidize(markup, categories_drop)).to eq('Our clothing products')
  end

  it "selects all categories" do
    create(:category, slug: 'cds', description: 'CDs')
    create(:category, slug: 'dvds', description: 'DVDs')
    categories_drop = CategoriesDrop.new

    expect(categories_drop.all.count).to eq(2)
  end

  it "selects root categories" do
    root_category = create(:category, slug: 'root')
    create(:category, parent: root_category)
    create(:category, parent: root_category)
    categories_drop = CategoriesDrop.new

    expect(categories_drop.roots.count).to eq(1)
    expect(categories_drop.roots.first.slug).to eq('root')
  end

  private

  def liquidize(markup, drop)
    Liquid::Template.parse(markup).render('categories'=>drop)
  end
end
