require "rails_helper"

describe SettingsDrop do
  it "allows accessing settings by name" do
    design = create(:design)
    create(:design_setting, design: design, name: 'background_color', value: 'white')
    settings_drop = SettingsDrop.new(design)
    markup = "{{ settings.background_color }}"

    expect(liquidize(markup, settings_drop)).to eq('white')
  end

  private

  def liquidize(markup, drop)
    Liquid::Template.parse(markup).render('settings'=>drop)
  end
end
