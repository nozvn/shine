# == Schema Information
#
# Table name: orders
#
#  id                  :integer          not null, primary key
#  number              :string
#  email               :string
#  ip_address          :string
#  billing_address_id  :integer
#  shipping_address_id :integer
#  completed_at        :datetime
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

FactoryGirl.define do
  factory :order do
    billing_address
    shipping_address

    email "customer@example.com"
  end
end
