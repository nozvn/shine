# == Schema Information
#
# Table name: variants
#
#  id            :integer          not null, primary key
#  product_id    :integer
#  price         :integer
#  is_master     :boolean          default(FALSE)
#  option1_value :string
#  option2_value :string
#  option3_value :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

FactoryGirl.define do
  factory :variant, aliases: [:master] do
    product
    price 10000

    factory :variant_inventory_tracked do
      track_inventory true

      factory :variant_backorder_allowed do
        allow_backorder true
      end
    end
  end
end
