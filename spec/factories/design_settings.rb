# == Schema Information
#
# Table name: design_settings
#
#  id         :integer          not null, primary key
#  design_id  :integer
#  name       :string
#  value      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :design_setting do
    design
    name "text-color"
    value "blue"
  end
end
