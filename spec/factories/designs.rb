# == Schema Information
#
# Table name: designs
#
#  id          :integer          not null, primary key
#  shop_id     :integer
#  name        :string
#  slug        :string
#  description :text
#  author      :string
#  email       :string
#  website     :string
#  version     :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :design do
    name "Beautiful"
  end
end
