# == Schema Information
#
# Table name: addresses
#
#  id         :integer          not null, primary key
#  first_name :string
#  last_name  :string
#  address1   :string
#  address2   :string
#  phone      :string
#  city       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :address, aliases: [:billing_address, :shipping_address] do
    first_name "John"
    last_name "Doe"
    address1 "123 Dove Street"
    phone "678-1234789"
    city "New York"
  end
end
