FactoryGirl.define do
  factory :order_item do
    order
    variant
    quantity 1
  end
end
