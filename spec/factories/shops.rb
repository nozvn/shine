FactoryGirl.define do
  factory :shop do
    title "My Shop"
    subdomain "myshop"
  end
end
