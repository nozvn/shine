# == Schema Information
#
# Table name: categories
#
#  id          :integer          not null, primary key
#  name        :string
#  slug        :string
#  description :string
#  parent_id   :integer
#  lft         :integer
#  rgt         :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :category do
    name "My category"

    factory :category_with_children do
      transient do
        children_count 3
      end

      after(:create) do |cat, evaluator|
        create_list(:category,
          evaluator.children_count, parent: cat)
      end
    end

    factory :category_with_products do
      transient do
        products_count 3
      end

      after(:create) do |c, evaluator|
        products = create_list(:product, evaluator.products_count)
        products.each do |p|
          Categorization.create(product: p, category: c)
        end
      end
    end
  end
end
