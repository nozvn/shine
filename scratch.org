* Shine Ecommerce Project

** DONE Make oauth server, write test cases.
   - Implement an oauth service. Use it to authorize store owner
     actions. Write test cases to ensure this works.
   - Make a basic oauth app to test against the created service.
   - Implement oauth App Management

** TODO Extend customer order functionality.
   - New customer when order is placed, find or create by email.
   - Customers receive marketing emails. Allow them to opt-out.

** DONE Scaffold React admin app.
** TODO Product image upload API
** TODO Design & Theme API
** TODO Orders API
** TODO Settings API
