class ReservedSubdomain
  def self.all
    text_file = Rails.root.join('config/reserved_subdomains.txt')
    File.readlines(text_file).map(&:chomp)
  end
end
