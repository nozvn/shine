module Constraints
  module Subdomain
    class Shop
      def self.matches?(request)
        request.subdomain.present? && request.subdomain != 'www'
      end
    end
  end
end
