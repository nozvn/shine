module Constraints
  module Subdomain
    class Checkout
      def self.matches?(request)
        request.subdomain == "checkout"
      end
    end
  end
end
